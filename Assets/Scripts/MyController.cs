﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MyController : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    public float fuerzaSalto = 5.0f;
    [SerializeField] Animator anim;
    private bool isDead = false;

    [SerializeField] AudioSource audio;

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (isDead) { return; }

        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        anim.SetFloat("velocidadX", Mathf.Abs(rb2d.velocity.x));
    }

    private void Update()
    {
        if (isDead) { return; }
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);

        if (jump)
        {
            Debug.LogError("Salta");
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Mortal")
        {
            Debug.Log("Muerte");
            anim.SetTrigger("muerte");
            isDead = true;
        }
        else if (collision.gameObject.tag == "PowerUp")
        {
            Debug.Log("Has pillado un powerup");
            maxS *= 4;
            Destroy(collision.gameObject);
        }
    }

    public void SonidoMuerte()
    {
        audio.Play();
    }
}